# Partie 1 : Partitionnement du serveur de stockage

## Partitionner le disque à l'aide de LVM

***créer un physical volume (PV) : le nouveau disque ajouté à la VM***

```
[user1@localhost ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sr0          11:0    1 1024M  0 rom  
vda         252:0    0    8G  0 disk 
├─vda1      252:1    0  600M  0 part /boot/efi
├─vda2      252:2    0    1G  0 part /boot
└─vda3      252:3    0  6.4G  0 part 
  ├─rl-root 253:0    0  5.6G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
vdb         252:16   0    2G  0 disk 
```
***créer un nouveau volume group (VG)***
```
[user1@localhost ~]$ sudo vgcreate storage /dev/vdb
[sudo] password for user1: 
  Physical volume "/dev/vdb" successfully created.
  Volume group "storage" successfully created
[user1@localhost ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sr0          11:0    1 1024M  0 rom  
vda         252:0    0    8G  0 disk 
├─vda1      252:1    0  600M  0 part /boot/efi
├─vda2      252:2    0    1G  0 part /boot
└─vda3      252:3    0  6.4G  0 part 
  ├─rl-root 253:0    0  5.6G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
vdb         252:16   0    2G  0 disk 
[user1@localhost ~]$ sudo vgs
  VG      #PV #LV #SN Attr   VSize  VFree 
  rl        1   2   0 wz--n-  6.41g     0 
  storage   1   0   0 wz--n- <2.00g <2.00g
```
***créer un nouveau logical volume (LV) : ce sera la partition utilisable***
```
[user1@localhost ~]$ sudo lvcreate -l 100%FREE storage -n last_storage
[sudo] password for user1: 
  Logical volume "last_storage" created.
[user1@localhost ~]$ lsblk
NAME                   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sr0                     11:0    1 1024M  0 rom  
vda                    252:0    0    8G  0 disk 
├─vda1                 252:1    0  600M  0 part /boot/efi
├─vda2                 252:2    0    1G  0 part /boot
└─vda3                 252:3    0  6.4G  0 part 
  ├─rl-root            253:0    0  5.6G  0 lvm  /
  └─rl-swap            253:1    0  820M  0 lvm  [SWAP]
vdb                    252:16   0    2G  0 disk 
└─storage-last_storage 253:2    0    2G  0 lvm  
```
***Formater la partition***
```
[user1@localhost storage]$ sudo mount /dev/storage/last_storage /mnt/storage
sudo mount /dev/storage/last_storage /mnt/storage
[user1@localhost storage]$ df -h
Filesystem                        Size  Used Avail Use% Mounted on
devtmpfs                          460M     0  460M   0% /dev
tmpfs                             476M     0  476M   0% /dev/shm
tmpfs                             191M  3.2M  188M   2% /run
/dev/mapper/rl-root               5.6G  1.2G  4.5G  20% /
/dev/vda2                        1014M  200M  815M  20% /boot
/dev/vda1                         599M  7.0M  592M   2% /boot/efi
tmpfs                              96M     0   96M   0% /run/user/1001
/dev/mapper/storage-last_storage  2.0G   24K  1.9G   1% /mnt/storage
```
```
[user1@localhost storage]$ vim /etc/fstab
[user1@localhost storage]$ sudo vim /etc/fstab
[sudo] password for user1: 
[user1@localhost storage]$ sudo umount /mnt/storage
[user1@localhost storage]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
/boot/efi                : already mounted
none                     : ignored
mount: /mnt/storage does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/storage             : successfully mounted
```

## Partie 2 : Serveur de partage de fichiers

***Donnez les commandes réalisées sur le serveur NFS storage.tp4.linux***

### Storage:

```
[user1@localhost ~]$ sudo dnf install nfs-utils
[user1@localhost ~]$ sudo mkdir /var/nfs/storage -p
[user1@localhost ~]$ ls -dl /var/nfs/storage
drwxr-xr-x 2 root root 4096 Apr 17 23:51 /var/nfs/storage
[user1@localhost ~]$ sudo chown nobody /var/nfs/storage
[user1@localhost ~]$ sudo dnf install nano
[user1@localhost ~]$ sudo nano /etc/exports
/var/nfs/storage    192.168.212.13(rw,sync,no_subtree_check)
/home               192.168.212.13(rw,sync,no_root_squash,no_subtree_check)

[user1@localhost ~]$ sudo systemctl enable nfs-server
[user1@localhost ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
     Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendo>
    Drop-In: /run/systemd/generator/nfs-server.service.d
             └─order-with-mounts.conf
     Active: active (exited) since Tue 2023-01-03 15:32:58 CET; 57min left
   Main PID: 896 (code=exited, status=0/SUCCESS)
        CPU: 19ms

Jan 03 15:32:57 localhost.localdomain systemd[1]: Starting NFS server and servi>
Jan 03 15:32:58 localhost.localdomain systemd[1]: Finished NFS server and servi>
...skipping...
[user1@localhost ~]$ sudo firewall-cmd --permanent --list-all | grep services
services: cockpit dhcpv6-client mountd nfs rpc-bind ssh

```

### Client:

```
[user1@localhost ~]$ sudo dnf install nfs-utils
[user1@localhost ~]$ sudo mkdir -p /nfs/storage
[user1@localhost ~]$ sudo mkdir -p /nfs/home
```
